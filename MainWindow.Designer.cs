﻿namespace SNMPUI
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpSNMP = new System.Windows.Forms.GroupBox();
            this.txtSNMPDesc = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.rdbEnable = new System.Windows.Forms.RadioButton();
            this.rdbDisable = new System.Windows.Forms.RadioButton();
            this.grpSNMP.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpSNMP
            // 
            this.grpSNMP.Controls.Add(this.txtSNMPDesc);
            this.grpSNMP.Location = new System.Drawing.Point(12, 12);
            this.grpSNMP.Name = "grpSNMP";
            this.grpSNMP.Size = new System.Drawing.Size(432, 135);
            this.grpSNMP.TabIndex = 0;
            this.grpSNMP.TabStop = false;
            this.grpSNMP.Text = "SNMP";
            // 
            // txtSNMPDesc
            // 
            this.txtSNMPDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSNMPDesc.Location = new System.Drawing.Point(6, 19);
            this.txtSNMPDesc.Multiline = true;
            this.txtSNMPDesc.Name = "txtSNMPDesc";
            this.txtSNMPDesc.Size = new System.Drawing.Size(420, 110);
            this.txtSNMPDesc.TabIndex = 0;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(15, 150);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(19, 13);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.Text = "__";
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(363, 227);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.OnClose);
            // 
            // rdbEnable
            // 
            this.rdbEnable.AutoSize = true;
            this.rdbEnable.Location = new System.Drawing.Point(12, 178);
            this.rdbEnable.Name = "rdbEnable";
            this.rdbEnable.Size = new System.Drawing.Size(58, 17);
            this.rdbEnable.TabIndex = 3;
            this.rdbEnable.TabStop = true;
            this.rdbEnable.Text = "Enable";
            this.rdbEnable.UseVisualStyleBackColor = true;
            this.rdbEnable.Click += new System.EventHandler(this.OnEnable);
            // 
            // rdbDisable
            // 
            this.rdbDisable.AutoSize = true;
            this.rdbDisable.Location = new System.Drawing.Point(104, 178);
            this.rdbDisable.Name = "rdbDisable";
            this.rdbDisable.Size = new System.Drawing.Size(60, 17);
            this.rdbDisable.TabIndex = 4;
            this.rdbDisable.TabStop = true;
            this.rdbDisable.Text = "Disable";
            this.rdbDisable.UseVisualStyleBackColor = true;
            this.rdbDisable.Click += new System.EventHandler(this.OnDisable);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(456, 262);
            this.Controls.Add(this.rdbDisable);
            this.Controls.Add(this.rdbEnable);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.grpSNMP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SNMP UI";
            this.Shown += new System.EventHandler(this.OnFirstShown);
            this.grpSNMP.ResumeLayout(false);
            this.grpSNMP.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpSNMP;
        private System.Windows.Forms.TextBox txtSNMPDesc;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.RadioButton rdbEnable;
        private System.Windows.Forms.RadioButton rdbDisable;
    }
}

