﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SNMPUI
{
    public partial class MainWindow : Form
    {
        StringCollection strlist = new StringCollection();

        public MainWindow()
        {
            InitializeComponent();
            txtSNMPDesc.Text = "";
        }

        private void OnFirstShown(object sender, EventArgs e)
        {
            ShellDism("Reading WMI database ...", "/Online /Get-FeatureInfo /FeatureName:SNMP /English");
        }

        private void OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e != null)
            {
                if (e.Data != null)
                {
                    if (e.Data.Length > 1)
                    {
                        strlist.Add(e.Data);
                    }
                }
            }
        }

        private void OnClose(object sender, EventArgs e)
        {
            Close();
        }

        private void OnEnable(object sender, EventArgs e)
        {
            ShellDism("Modifying WMI database ...", "/online /Enable-Feature /FeatureName:SNMP /English");
            lblStatus.Text = "Status: Enabled";
            MessageBox.Show("Your must reboot your computer for these changes to take effect");
        }

        private void OnDisable(object sender, EventArgs e)
        {
            ShellDism("Modifying WMI database ...", "/online /Disable-Feature /FeatureName:SNMP /English");
            lblStatus.Text = "Status: Disabled";
            MessageBox.Show("Your must reboot your computer for these changes to take effect");
        }

        void ShellDism(String lbltext, String arguements)
        {
            btnClose.Enabled = false;
            rdbEnable.Enabled = false;
            rdbDisable.Enabled = false;

            lblStatus.Text = lbltext;

            strlist.Clear();
            Process snmpproc = new Process();
            snmpproc.StartInfo.FileName = "dism";
            snmpproc.StartInfo.Arguments = arguements;
            snmpproc.StartInfo.UseShellExecute = false;
            snmpproc.StartInfo.RedirectStandardOutput = true;
            snmpproc.StartInfo.CreateNoWindow = true;
            snmpproc.OutputDataReceived += OutputDataReceived;
            snmpproc.Start();
            snmpproc.BeginOutputReadLine();
            snmpproc.WaitForExit();

            foreach (String str in strlist)
            {
                if (str.Contains("Display Name") || str.Contains("Description"))
                {
                    txtSNMPDesc.Text += str + "\r\n";
                }

                if (str.Contains("State"))
                {
                    lblStatus.Text = str;

                    if (str.Contains("Enabled"))
                    {
                        rdbDisable.Checked = false;
                        rdbEnable.Checked = true;
                    }
                    else
                    {
                        rdbDisable.Checked = true;
                        rdbEnable.Checked = false;
                    }
                }
            }

            btnClose.Enabled = true;
            rdbEnable.Enabled = true;
            rdbDisable.Enabled = true;
        }
    }
}
